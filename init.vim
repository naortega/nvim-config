set nocompatible
filetype off
call plug#begin("~/.config/nvim/plugged")
Plug 'tpope/vim-fugitive'
Plug 'vim-scripts/DoxygenToolkit.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdcommenter'
Plug 'javier-lopez/sprunge.vim'
Plug 'vim-scripts/Trailer-Trash'
Plug 'vim-scripts/bad-whitespace'
Plug 'junegunn/fzf'
Plug 'aklt/plantuml-syntax'
Plug 'vim-scripts/scons.vim'
Plug 'danro/rename.vim'
Plug 'ciaranm/detectindent'
Plug 'jiangmiao/auto-pairs'
Plug 'antoyo/vim-licenses'
Plug 'altercation/vim-colors-solarized'
"Plug 'ervandew/supertab'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
call plug#end()
filetype plugin indent on
" to ignore plugin indent changes, instead use:
filetype plugin on

""" General
" indentation
set tabstop=4                " tabs only count for 4 spaces
set shiftwidth=4             " when indenting use 4 spaces
set noexpandtab              " use tab characters
set autoindent

" view
set number
set showmatch                " show matching bracket
set incsearch                " search as characters are typed
set hlsearch                 " highlight found matches
set incsearch nohlsearch     " turn off hightlight after search
set wrap
set linebreak
set nolist                   " disable line break on wrap

" folding
set foldmethod=syntax
"set foldlevel=0
"set foldnestmax=1

" CoC settings
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"
"inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
nmap <C-a> :CocAction<CR>
let g:coc_disable_startup_warning = 1

inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
	let col = col('.') - 1
	return !col || getline('.')[col - 1]  =~# '\s'
endfunction

func! s:my_colors_setup() abort
	" this is an example
	hi CocFloating cterm=reverse ctermfg=251
	hi CocMenuSel cterm=reverse ctermfg=255
endfunc

augroup colorscheme_coc_setup | au!
	au ColorScheme * call s:my_colors_setup()
augroup END

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" syntax highlighting
syntax on
set background=dark
colorscheme solarized

""" Project-Specific Settings

set cino+=(0
set cino+=:0
set cino+=l1
set cino+=g0
set cino+=N-s
set cino+=E-s
set cino+=p2s
set cino+=t0

""" key remapping
" cursor navigation
"nmap j gj
"nmap k gk
"vmap j gj
"vmap k gk

" tab navigation
nmap <S-J> :tabn<CR>
nmap <S-K> :tabp<CR>

" Rust
let g:rust_recommended_style = 0   " disable retarded option

""" plugin settings
" airline
let g:airline#extensions#tabline#enabled = 1   " enable tabline
let g:airline_theme='luna'
if !exists('g:airline_symbols')
	let g:airline_symbols = {}
endif
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.whitespace = 'Ξ'
let g:airline#extensions#whitespace#mixed_indent_algo = 2
let g:airline#extensions#whitespace#checks = [ 'indent', 'long']

" vim-licenses
let g:licenses_copyright_holders_name = 'Ortega Froysa, Nicolás <nicolas@ortegas.org>'
let g:licenses_authors_name = 'Ortega Froysa, Nicolás <nicolas@ortegas.org>'
let g:licenses_default_command = [ 'Affero' ]

" FZF
nnoremap <leader>f :FZF<CR>
nnoremap <leader>F :tabnew<CR>:FZF<CR>

" command aliases
command! Gcommit Git commit -v
